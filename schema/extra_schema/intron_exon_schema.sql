CREATE TABLE contigs (
       tax_id int NOT NULL,
       chr VARCHAR(127) NOT NULL,
       chr_start INT NOT NULL,
       chr_stop INT NOT NULL,
       orientation VARCHAR(1) NOT NULL DEFAULT '+',
       feature_name VARCHAR(127) NOT NULL,
       gi INT,
       feature_type VARCHAR(127) NOT NULL
);

CREATE TABLE exons (
       gi int NOT NULL,
       accession VARCHAR(127) NOT NULL,
       accession_ver INT NOT NULL,
       number INT NOT NULL,
       start INT NOT NULL,
       stop INT NOT NULL	
       );

CREATE TABLE cds (
       gi int NOT NULL,
       accession VARCHAR(127) NOT NULL,
       accession_ver INT NOT NULL,
       variant VARCHAR(127),
       complement BOOLEAN DEFAULT FALSE,
       gene VARCHAR (127),
       geneid INT NOT NULL,
       contig_gi INT NOT NULL,
       chr VARCHAR (127),
       number INT NOT NULL,
       phase INT NOT NULL,
       start INT NOT NULL,
       stop INT NOT NULL,
       contig_start INT NOT NULL,	
       contig_stop INT NOT NULL,
       position INT NOT NULL);

CREATE TABLE mrna (
       gi int NOT NULL,
       accession VARCHAR(127) NOT NULL,
       accession_ver INT NOT NULL,
       variant INT,
       complement BOOLEAN DEFAULT FALSE,
       gene VARCHAR (127),
       geneid INT NOT NULL,
       contig_gi INT NOT NULL,
       chr VARCHAR (127),
       exon BOOLEAN DEFAULT TRUE,
       number INT NOT NULL,
       start INT NOT NULL,
       stop INT NOT NULL,
       contig_start INT NOT NULL,	
       contig_stop INT NOT NULL);


