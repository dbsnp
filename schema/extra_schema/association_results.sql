DROP VIEW ga_snp;
DROP VIEW ga_chr_pos;
DROP TABLE ga_study_snp CASCADE;
DROP TABLE ga_study_subpart CASCADE;
DROP TABLE ga_study CASCADE;


CREATE TABLE ga_study (
       ga_id       SERIAL PRIMARY KEY,
       short_name  TEXT NOT NULL,
       full_name   TEXT NOT NULL,
       description TEXT NOT NULL DEFAULT '',
       date 	   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE UNIQUE INDEX ON ga_study(short_name);

CREATE TABLE ga_study_subpart (
       ga_subpart_id SERIAL PRIMARY KEY,
       ga_id INT NOT NULL REFERENCES ga_study ON DELETE CASCADE,
       short_name TEXT NOT NULL,
       full_name TEXT NOT NULL,
       description TEXT NOT NULL DEFAULT ''
);

CREATE UNIQUE INDEX ON ga_study_subpart(ga_id,short_name);
CREATE INDEX ON ga_study_subpart(ga_id);

CREATE TABLE ga_study_snp (
       snp_id INT NOT NULL,
       ga_subpart_id INT NOT NULL REFERENCES ga_study_subpart ON DELETE CASCADE,
       pvalue double precision,
       qvalue double precision,
       fdr double precision,
       significant BOOLEAN DEFAULT FALSE
);

CREATE UNIQUE INDEX ON ga_study_snp(snp_id,ga_subpart_id);
CREATE INDEX ON ga_study_snp(snp_id);
CREATE INDEX ON ga_study_snp(fdr);
CREATE INDEX ON ga_study_snp(qvalue);
CREATE INDEX ON ga_study_snp(pvalue);

CREATE TABLE ga_study_chr_pos (
       chr varchar NOT NULL,
       pos int NOT NULL,
       ga_subpart_id INT NOT NULL REFERENCES ga_study_subpart ON DELETE CASCADE,
       pvalue double precision,
       qvalue double precision,
       fdr double precision,
       significant BOOLEAN DEFAULT FALSE
);

CREATE UNIQUE INDEX ON ga_study_chr_pos(chr,pos,ga_subpart_id);
CREATE INDEX ON ga_study_chr_pos(chr,pos);
CREATE INDEX ON ga_study_chr_pos(fdr);
CREATE INDEX ON ga_study_chr_pos(qvalue);
CREATE INDEX ON ga_study_chr_pos(pvalue);



CREATE OR REPLACE VIEW ga_snp AS
       SELECT gss.snp_id AS snp_id,
              gs.short_name AS study_name,
              gssp.short_name AS subpart_name,
              gss.pvalue AS pvalue,
              gss.qvalue AS qvalue,
              gss.fdr AS fdr,
              gss.significant AS significant
              FROM ga_study_snp gss
         JOIN ga_study_subpart gssp ON gss.ga_subpart_id = gssp.ga_subpart_id
         JOIN ga_study gs ON gssp.ga_id=gs.ga_id;

CREATE OR REPLACE VIEW ga_chr_pos AS
       SELECT gsc.chr AS chr,
              gsc.pos AS pos,
              gs.short_name AS study_name,
              gssp.short_name AS subpart_name,
              gsc.pvalue AS pvalue,
              gsc.qvalue AS qvalue,
              gsc.fdr AS fdr,
              gsc.significant AS significant
              FROM ga_study_chr_pos gsc
         JOIN ga_study_subpart gssp ON gsc.ga_subpart_id = gssp.ga_subpart_id
         JOIN ga_study gs ON gssp.ga_id=gs.ga_id;

