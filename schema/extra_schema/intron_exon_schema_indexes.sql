CREATE UNIQUE INDEX ON contigs(gi);
CREATE INDEX ON contigs(feature_name);
CREATE INDEX ON contigs(chr);

CREATE UNIQUE INDEX ON exons(gi,number);
CREATE UNIQUE INDEX ON exons(accession,accession_ver,number);
CREATE INDEX ON exons(accession);

CREATE UNIQUE INDEX ON cds(gi,number);
CREATE UNIQUE INDEX ON cds(accession,accession_ver,number);
CREATE INDEX ON cds(accession);

CREATE INDEX ON cds(accession);
CREATE INDEX ON cds(gene);
CREATE INDEX ON cds(start);
CREATE INDEX ON cds(stop);
CREATE INDEX ON cds(gi);
CREATE INDEX ON cds(start,stop);
CREATE INDEX ON cds(chr);

-- CREATE UNIQUE INDEX ON mrna(accession,ctg_gi,number);
-- CREATE INDEX ON mrna(gi,number);
-- CREATE INDEX ON mrna(accession,accession_ver,number);
CREATE INDEX ON mrna(accession);
CREATE INDEX ON mrna(gene);
CREATE INDEX ON mrna(start);
CREATE INDEX ON mrna(stop);
CREATE INDEX ON mrna(gi);
CREATE INDEX ON mrna(start,stop);
CREATE INDEX ON mrna(chr);