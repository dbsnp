CREATE TABLE ideogram (
       chr TEXT NOT NULL,
       pq  TEXT NOT NULL,
       ideogram TEXT NOT NULL,
       rstart INT NOT NULL, -- I think these are recombination rates,
			    -- but I'm not sure
       rstop INT NOT NULL,
       start INT NOT NULL,
       stop INT NOT NULL,
       posneg TEXT NOT NULL -- I believe this indicates whether the
			    -- band is black or white
       );
       
CREATE UNIQUE INDEX ON ideogram(chr,pq,ideogram);
CREATE UNIQUE INDEX ON ideogram(chr,start);
CREATE UNIQUE INDEX ON ideogram(chr,stop);
