CREATE TABLE mrna_cds_table (
       gi int NOT NULL,
       cds_gi INT,
       accession TEXT NOT NULL,
       accession_ver INT NOT NULL,
       gene TEXT NOT NULL,
       start INT NOT NULL,
       stop INT NOT NULL,
       length INT NOT NULL,
       sequence TEXT NOT NULL,
       prot_seq TEXT NOT NULL
);

CREATE UNIQUE INDEX ON mrna_cds_table(gi);
CREATE INDEX ON mrna_cds_table(cds_gi);
CREATE UNIQUE INDEX ON mrna_cds_table(accession,accession_ver);
CREATE INDEX ON mrna_cds_table(gene);