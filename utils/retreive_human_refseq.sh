#!/bin/sh

rsync -rvP -m --include '**GRCh37.p*_*.gbs.gz' --include '**GRCh37.p*_*.fa.gz' --include '**/' --exclude '*' rsync://ftp.ncbi.nlm.nih.gov/genomes/H_sapiens/CHR* .
rsync -rvP rsync://ftp.ncbi.nlm.nih.gov/refseq/H_sapiens/mRNA_Prot/human.rna.gbff.gz .
rsync -LvP rsync://ftp.ncbi.nlm.nih.gov/genomes/H_sapiens/mapview/seq_contig.md.gz .
