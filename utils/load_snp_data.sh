#!/bin/sh

DATA_DIR=/srv/ncbi/db_snp/
SCHEMA_DIR=/srv/ncbi/db_snp_utils/schema
UTIL_DIR=${SCHEMA_DIR}/../utils/
DB=snp

psql -c "DROP DATABASE $DB";
psql -c "CREATE DATABASE $DB";



(cd ${SCHEMA_DIR}/shared_schema;
    cat dbSNP_main_table_postgresql.sql |psql "$DB";
)
(cd ${SCHEMA_DIR}/human_9606_schema;
    cat *_table_postgresql.sql|psql "$DB";
    ${UTIL_DIR}/human_gty1_indexes_creation.pl create trigger |psql "$DB";
)
(cd ${DATA_DIR}/shared_data;
    for a in $(find -type f -iname '*.bcp.gz' -printf '%f\n'|sort); do
	echo $a;
	zcat $a | perl -pe 's/\r/\\r/g' |psql "$DB" -c "COPY ${a%%.bcp.gz} FROM STDIN WITH NULL ''";
    done;
)
(cd ${DATA_DIR}/organism_data;
    for a in $(find -type f -iname '*.bcp.gz' -printf '%f\n'|sort); do
	echo $a;
	zcat $a | perl -pe 's/\r/\\r/g' |psql "$DB" -c "COPY ${a%%.bcp.gz} FROM STDIN WITH NULL ''";
    done;
)
(cd ${SCHEMA_DIR}/shared_schema;
    cat dbSNP_main_index_postgresql.sql dbSNP_main_constraint_postgresql.sql|psql "$DB";
)
(cd ${SCHEMA_DIR}/human_9606_schema;
    cat *_{index,constraint}_postgresql.sql|psql "$DB";
    ${UTIL_DIR}/human_gty1_indexes_creation.pl index |psql "$DB";
)
