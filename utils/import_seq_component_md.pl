#!/usr/bin/perl

use warnings;
use strict;

while (<>) {
    next if /^#/;
    chomp;
    my @row = split /\t/;
    pop @row;
    pop @row;
    $row[6] =~ s/na/NULL/;
    print join("\t",@row)."\n";
}
