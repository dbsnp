#!/bin/sh

DATA_DIR=/srv/ncbi/refseq/
SCHEMA_DIR=/srv/ncbi/db_snp_utils/schema
UTIL_DIR=${SCHEMA_DIR}/../utils/


psql snp < ${SCHEMA_DIR}/extra_schema/mrna_cds_table.sql
psql snp < ${SCHEMA_DIR}/extra_schema/intron_exon_schema.sql

zcat ${DATA_DIR}/human.rna.gbff.gz | \
    ${UTIL_DIR}/human_mrna_cds_insert.pl | \
    psql snp -c "COPY mrna_cds_table FROM STDIN WITH DELIMITER '	' NULL AS 'NULL'";

zcat ${DATA_DIR}/seq_contig.md.gz | \
    ${UTIL_DIR}/import_seq_component_md.pl | \
    psql snp -c "COPY contigs FROM STDIN WITH DELIMITER '	' NULL AS 'NULL'";

zcat ${DATA_DIR}/CHR_*/hs_ref_GRCh37.p*_*.gbs.gz | \
    ${UTIL_DIR}/intron_exon_loader.pl --type mrna| \
    psql snp -c "COPY mrna FROM STDIN WITH DELIMITER '	' NULL AS 'NULL'";

zcat ${DATA_DIR}/CHR_*/hs_ref_GRCh37.p*_*.gbs.gz | \
    ${UTIL_DIR}/intron_exon_loader.pl --type cds| \
    psql snp -c "COPY cds FROM STDIN WITH DELIMITER '	' NULL AS 'NULL'";
